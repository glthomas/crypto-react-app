using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Amazon.Lambda.Core;
using System.Net.Http; 

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
//[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace CSharpLambdaFunction
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        /// 

        private string data;        

        async Task<string> GetData()
        {
            //We will make a GET request to a really cool website...

            string baseUrl = "https://api.coinmarketcap.com/v2/listings/";
            //The 'using' will help to prevent memory leaks.
            //Create a new instance of HttpClient
            using (HttpClient client = new HttpClient())

            //Setting up the response...         

            using (HttpResponseMessage res = await client.GetAsync(baseUrl))
            using (HttpContent content = res.Content)
            {
                data = await content.ReadAsStringAsync();
                //
                // manipulate the data if need
                //
                return data;
            }
        }

        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
        public async Task<JObject> FunctionHandler(ILambdaContext context)
        {            
            string rtrnString = await GetData();
            JObject jobj = JsonConvert.DeserializeObject<JObject>(rtrnString);
            return jobj;
        }
    }
    
}
