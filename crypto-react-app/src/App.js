import React, { Component } from 'react';
import nmlogo from './spinner.svg';
import './App.css';
import Chartist from 'chartist';
import ChartistGraph from 'react-chartist';
import moment from 'moment';


/* ####################### */
/* ##### Crypto List ##### */
/* ####################### */
const CryptoList = ({
    data,
    filter,
    favourites,
    selectCrypto
}) => {
    const input = filter.toLowerCase()
    
    // Gather list of names
    const cryptos = data
        // .slice(0,100) un comment this to cut the number of cryptos down until pagination is implemented
        // filtering out the names that...
        .filter((crypto, i) => {
            return (                               
                // ...are not matching the current search value
                !crypto.name.toLowerCase().indexOf(input)
            )
        })
        .sort(function (a, b) {
            var textA = a.name.toUpperCase();
            var textB = b.name.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })
        // ...output a <Name /> component for each name
        .map((crypto, i) => {
            // only display names that match current input string
            return (
                <Name
                    id={crypto.id}
                    key={i}
                    info={crypto}
                    handleCrypto={() => selectCrypto(crypto)}
                />
            )
        })

    /* ##### the component's output ##### */
    return (
        <ul>
            {cryptos}
        </ul>
    )
}



/* ###################### */
/* ##### Filter bar ##### */
/* ###################### */
// need a component class here
// since we are using `refs`
class Search extends Component {
    render() {
        const { filterVal, filterUpdate } = this.props
        return (            
            <input
                type='text'
                ref='filterInput'
                placeholder='Type to filter..'
                // binding the input value to state
                value={filterVal}
                onChange={() => {
                    filterUpdate(this.refs.filterInput.value)
                }}
            />            
        )
    }
}




/* ###################### */
/* ##### AJAX Call  ##### */
/* ###################### */
const LoadCryptos = ({ apigClient, loadCryptos, isLoading }) => {
    console.log(isLoading);
    return ([
        <div key={1}>{!isLoading && [<h2 key={1}>Click the button below to load the latest Crypto Currencies</h2>,
        <button key={2} onClick={() => loadCryptos()}>
            Load Cryptocurrencies
            </button>]
        }
        </div>,
        <div key={2}>

            {
                isLoading && <div className="spinner-container">
                    <Spinner /><h2>Loading Cryptocurrencies</h2>
                    
                </div>
            }
        </div>
    ]
    )
}




/* ##################### */
/* #####  Spinner  ##### */
/* ##################### */
const Spinner = () => {
    return (
        <img src={nmlogo} className="App-logo" alt="logo2" />
    )
}




/* ############################ */
/* #### Single Crypto Name #### */
/* ############################ */
const Name = ({ id, info, handleCrypto }) =>
(
    <li
        className={info}
        onClick={() => handleCrypto(id)}
    >
        {info.name}
    </li>
)




/* ############################## */
/* ##### Main app component ##### */
/* ############################## */
class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            filterText: '',
            favourites: [],
            cryptos: [],
            loadingCryptos: false,
            selectedCrypto: null,
            lineChartData: []
        }
    }

    // update filterText in state when user types 
    filterUpdate(value) {
        this.setState({
            filterText: value
        });
    }

    ajaxLoadCryptoList() {
        var apigClient = this.props.data;
        var cryptos = [];
        this.setState({ loadingCryptos: true })

        apigClient.getcryptosGet().then((result) => {
            // Add success callback code here.            
            cryptos = result.data.data;
        }).then(() => this.setState(
            {
                cryptos: cryptos,
                loadingCryptos: false
            })
        );        
    }

    ajaxLoadSelectedCrypto(crypto) {
        // Bring user to top of page incase they are scrolled way down page
        
        window.scrollTo(0, 0);

        // Also clear out the chart data so we're not falsely advertising data from the previously selected crypto
        this.setState({ lineChartData: [] });

        var apigClient = this.props.data;       
        
        const parameters = {
            crypto: crypto.website_slug,
            tickstart: "1367174841000",
            tickend: "1526247272000"
        }

        apigClient.getcryptoGet(null, parameters, { queryParams: parameters }).then((result) => {            
            var mapped = result.data.map((a) => ({ x: new Date(a[0]), y: a[1]} ));
            this.setState({ lineChartData: mapped});
            
        });
    }

    // set the crypto to selected and go fetch...
    selectCrypto(crypto) {        
        this.setState({ selectedCrypto: crypto });
        this.ajaxLoadSelectedCrypto(crypto);
    }


    render() {

        const hasSearch = this.state.filterText.length > 0
        const hasCryptos = (this.state.cryptos.length > 0)
        const cryptoBeenSelected = this.state.selectedCrypto !== null;

        var lineChartData = {            
            series: [{
                name: 'series-1',
                data: this.state.lineChartData
            }]
        }
        var lineChartOptions = {
            low: 0,
            showArea: true,
            divisor: 5,
            axisX: {
                type: Chartist.FixedScaleAxis,
                divisor: 5,
                labelInterpolationFnc: function(value) {
                  return moment(value).format('MMM D YYYY');
                }
            }
        }            

        return (
            <div>
                <header className="App-header">
                    <Search
                        filterVal={this.state.filterText}
                        filterUpdate={this.filterUpdate.bind(this)}
                    /> 
                </header>
                
                {cryptoBeenSelected &&
                    <ChartistGraph data={lineChartData} options={lineChartOptions} type={'Line'} />
                }
                
                <main>
                    {(!cryptoBeenSelected && hasCryptos) &&
                        <h2>Select a Cryptocurrentcy</h2>
                    }
                    {cryptoBeenSelected && <h2 className="crypto-title">{this.state.selectedCrypto.name}</h2>}
                    {!hasCryptos &&
                        <LoadCryptos
                            apigClient={this.props.data}
                            loadCryptos={this.ajaxLoadCryptoList.bind(this)}
                            isLoading={this.state.loadingCryptos}
                        />
                    }

                    <CryptoList
                        data={this.state.cryptos}
                        filter={this.state.filterText}
                        favourites={this.state.favourites}
                        selectCrypto={this.selectCrypto.bind(this)}
                    />                    
                    {hasSearch &&
                        <button
                            onClick={this.filterUpdate.bind(this, '')}>
                            Clear Search
                        </button>
                    }                    
                </main>
            </div>
        )
    }
}

export default App;
