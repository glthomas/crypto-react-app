##**[n-m.app](https://n-m.app)**

For the Full Stack Engineer Mission / Challenge I created a responsive React Application which loads a list of Cryptocurrencies which can be quickly filtered via the input field at the top of the application.  In this BitBucket Repo you'll find three projects; **crypto-react-app**, **CSharpLambdaFunction**, and **NodeJsLambdaFunction**

1. When the app loads the user is prompted to click a button to load the latest cryptocurrencies.
2. Clicking the button makes an AJAX call through the Amazon API Gateway Javascript SDK to an Endpoint that I set up at AWS API Gateway.
3. The API Endpoint for the GetCryptos call triggers an Amazon Lambda function written in C# to make an asynchonous HTTP request to retrieve the list of cryptocurrencies from this public API [https://coinmarketcap.com/api/](https://coinmarketcap.com/api/)
4. While loading, the user is informed that the Cryptocurrencies are loading via message and loading spinner.
5. Once the Cryptocurrencies are loaded the user can either filter via input text at the top of the application or they can select a cryptocurrenty from the alphabetically sorted list of cryptocurrencies.
6. Filtering via text happens quickly.  
7. The user can aslo click / tab on a cryptocurrency.  Doing so will trigger an AJAX call to the API endpoint to get price data on the selected cryptocurrency.  This endpoint triggers an Amazon Lambda function this time written in node.js to make an asynchronous http request via the axios HTTP Provider library.  
8. Finally using a simple charting library called chartist I plot the history of US price information with the response data.

Since the new gTDL .app just became available last week I thought it would be nice to grab **[N-M.APP](https://n-m.app)** and host this application there for your convenience.  

---

