'use strict';
var axios = require('axios');

module.exports.hello = (event, context, callback) => {
    var url = "https://graphs2.coinmarketcap.com/currencies/" + event.query.crypto;
    axios.get(url)
        .then(function (response) {
            callback(null, response.data.price_usd);
        })
        .catch(function (error) {
            console.log(error);
            callback(null, {});
        });
};